FROM ubuntu:22.04

RUN apt-get update && apt-get install -y bash wget curl imagemagick

WORKDIR /tpentorno

COPY . .

CMD ["bash","/tpentorno/menu.sh"]

