read -p "Ingrese la URL del archivo comprimido de imágenes: " URL_COMP
read -p "Ingrese la URL del archivo de suma de verificación: " URL_SUMA

ARCHIVO=imagenes.tar.gz
SUMA_VER=sumaDeVerificacion.txt
#Si existen, los borra
rm $ARCHIVO $SUMA_VER 2> /dev/null 

#Descarga el archivo comprimido (tar.gz)
curl $URL_COMP > $ARCHIVO  
if [ $? -eq 0 ]; then
        echo "Archivo $ARCHIVO descargado"
else
        echo "Error al descargar $ARCHIVO"
	[ -f $ARCHIVO ] && rm $ARCHIVO
        return 1
fi

#Descarga el archivo con la suma de verificación (txt)
curl $URL_SUMA > $SUMA_VER  
if [ $? -eq 0 ]; then
        echo "Archivo $SUMA_VER descargado"
else
        echo "Error al descargar $SUMA_VER"
	[ -f $ARCHIVO ] && rm $ARCHIVO
	[ -f $SUMA_VER ] && rm $SUMA_VER
        return 2
fi

#Comprueba la suma de verificación
if [ $(sha256sum $ARCHIVO | cut -d " " -f 1) = $(cat $SUMA_VER) ]; then
	echo "Suma de verificación correcta"
	return 0
else
        echo "Error en la suma de verificación"
	[ -f $ARCHIVO ] && rm $ARCHIVO 
        [ -f $SUMA_VER ] && rm $SUMA_VER 
	return 3
fi
