#Si no existe la carpeta imagenes, la crea
[ -d  imagenes ] || mkdir imagenes

#Si existe y tiene imágenes las borra
rm imagenes/* 2> /dev/null

ARCHIVO="imagenes.tar.gz"
if [ -f $ARCHIVO ]; then
	#Descomprime (archivo.tar.gz -> archivo.tar) y desempaqueta (archivo.tar ->) el archivo
	tar -xzf $ARCHIVO --strip-components=1 -C imagenes/ 2> /dev/null
	if [ $? -eq 0 ]; then
        	echo "Archivo $ARCHIVO.tar.gz descomprimido"
		return 0
	else
        	echo "Error al descomprimir $ARCHIVO.tar.gz"
#	        [ -f $ARCHIVO.tar.gz ] && rm $ARCHIVO.tar.gz 
        	return 1
	fi
else
	echo "No existe $ARCHIVO.tar.gz"
	return 2
fi
