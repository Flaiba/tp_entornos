#Descarga el archivo mujeres.csv, limpia el contendo dejando todos los nombres con la primer letra en mayúsculas y el resto en minúsculas
curl https://raw.githubusercontent.com/adalessandro/EdP-2023-TP-Final/main/dict.csv | cut -d "," -f 1 | cut -d " " -f 1 | tr 'áéíóúàèìòù' 'aeiouaeiou' | sort | uniq | grep -E '[A-Z][a-z]*' -> nombres.txt

MAX_IMG=$(cat nombres.txt | wc -l)
#Pregunta al usuario la cantidad de imágenes a descargar
echo "Cantidad de imágenes que desea descargar"
read -p "Imágenes (entre 1 y $MAX_IMG): " NRO_IMG
while [[ $NRO_IMG -le 0 || $NRO_IMG -gt $MAX_IMG || ! $NRO_IMG =~ [[:digit:]]+ ]]; do
        echo "Número erróneo"
        read -p "Imágenes (entre 1 y $MAX_IMG_M): " NRO_IMG
done

#Si no existe la carpeta imagenes, la crea
[ -d  imagenes ] || mkdir imagenes

#Si existe y tiene archivos los borra
rm imagenes/*

for n in $(seq 1 $NRO_IMG); do
        #Elige un nombre en forma aleatoria
        NOMBRE=$(shuf -n 1 nombres.txt)
        #Comprueba que ese nombre no fue usado
        while [ -f "imagenes/$NOMBRE.jpg" ]; do
                NOMBRE=$(shuf -n 1 nombres.txt)
        done
	#Descarga una imagen y le asigna el nombre $NOMBRE al archivo jpg
	wget -O "imagenes/$NOMBRE.jpg" https://thispersondoesnotexist.com/
        sleep 2s
done

ARCHIVO=imagenes.tar.gz
SUMA_VER=sumaDeVerificacion.txt
#Si existen, los borra
rm $ARCHIVO $SUMA_VER 2> /dev/null 

#Empaqueta (tar) y comprime (gz) las imágenes
tar -czf $ARCHIVO imagenes/*.jpg 2> /dev/null 
if [ $? -eq 0 ]; then
        echo "Archivo $ARCHIVO creado"
	#Borra las imágenes después de generar el archivo comprimido
	rm imagenes/*.jpg
else
        echo "Error al crear $ARCHIVO"
        [ -f $ARCHIVO ] && rm $ARCHIVO
	return 1
fi

#Crea el archivo de suma de verificación
sha256sum $ARCHIVO | cut -d " " -f 1 > $SUMA_VER
if [ $? -eq 0 ]; then
        echo "Archivo $SUMA_VER creado"
	return 0
else
        echo "Error al crear $SUMA_VER"
        [ -f $ARCHIVO ] && rm $ARCHIVO
        [ -f $SUMA_VER ] && rm $SUMA_VER
	return 2
fi
