#Descarga el archivo mujeres.csv, limpia el contendo dejando todos los nombres de mujeres que terminan con "a"
curl https://raw.githubusercontent.com/marcboquet/spanish-names/master/mujeres.csv | cut -d "," -f 1 | cut -d " " -f 1 | sort | uniq | tr [[:upper:]] [[:lower:]] | sed 's/\b\(.\)/\u\1/' | grep -E '*a$' > nombres_mujeres.txt

#Descarga el archivo hombres.csv, limpia el contendo dejando todos los nombres de hombres que terminan con "o"
curl https://raw.githubusercontent.com/marcboquet/spanish-names/master/hombres.csv | cut -d "," -f 1 | cut -d " " -f 1 | sort | uniq | tr [[:upper:]] [[:lower:]] | sed 's/\b\(.\)/\u\1/' | grep -E '*o$' > nombres_hombres.txt

MAX_IMG_M=$(cat nombres_mujeres.txt | wc -l)
MAX_IMG_H=$(cat nombres_hombres.txt | wc -l)
#Pregunta al usuario la cantidad de imágenes a descargar
echo "Cantidad de imágenes que desea descargar"
read -p "Imágenes de mujeres (entre 1 y $MAX_IMG_M): " NRO_IMG_M
while [[ $NRO_IMG_M -le 0 || $NRO_IMG_M -gt $MAX_IMG_M || ! $NRO_IMG_M =~ [[:digit:]]+ ]]; do
        echo "Número erróneo"
        read -p "Imágenes de mujeres (entre 1 y $MAX_IMG_M): " NRO_IMG_M
done
read -p "Imágenes de hombres (entre 1 y $MAX_IMG_H): " NRO_IMG_H
while [[ $NRO_IMG_H -le 0 || $NRO_IMG_H -gt $MAX_IMG_H || ! $NRO_IMG_H =~ [[:digit:]]+ ]]; do
        echo "Número erróneo"
        read -p "Imágenes de hombres (entre 1 y $MAX_IMG_H):" NRO_IMG_H
done

#Si no existe la carpeta imagenes, la crea
[ -d  imagenes ] || mkdir imagenes

#Si existe y tiene archivos los borra
rm imagenes/*

for n in $(seq 1 $NRO_IMG_M); do
        #Elige un nombre en forma aleatoria
        NOMBRE=$(shuf -n 1 nombres_mujeres.txt)
        #Comprueba que ese nombre no fue usado
        while [ -f "imagenes/$NOMBRE.jpg" ]; do
                NOMBRE=$(shuf -n 1 nombres_mujeres.txt)
        done
	#Descarga una imagen y le asigna el nombre $NOMBRE al archivo jpg
	#wget -O "imagenes/$NOMBRE.jpg" https://thispersondoesnotexist.com/
	wget -O "imagenes/$NOMBRE.jpg" https://source.unsplash.com/random/900%C3%97700/?woman
        sleep 2s
done
for n in $(seq 1 $NRO_IMG_H); do
        #Elige un nombre en forma aleatoria
        NOMBRE=$(shuf -n 1 nombres_hombres.txt)
        #Comprueba que ese nombre no fue usado
        while [ -f "imagenes/$NOMBRE.jpg" ]; do
                NOMBRE=$(shuf -n 1 nombres_hombres.txt)
        done
        #Descarga una imagen y le asigna el nombre $NOMBRE al archivo jpg
	#wget -O "imagenes/$NOMBRE.jpg" https://thispersondoesnotexist.com/ 
	wget -O "imagenes/$NOMBRE.jpg" https://source.unsplash.com/random/900%C3%97700/?man
	sleep 2s
done

ARCHIVO=imagenes.tar.gz
SUMA_VER=sumaDeVerificacion.txt
#Si existen, los borra
rm $ARCHIVO $SUMA_VER 2> /dev/null 

#Empaqueta (tar) y comprime (gz) las imágenes
tar -czf $ARCHIVO imagenes/*.jpg 2> /dev/null 
if [ $? -eq 0 ]; then
        echo "Archivo $ARCHIVO creado"
	#Borra las imágenes después de generar el archivo comprimido
	rm imagenes/*.jpg
else
        echo "Error al crear $ARCHIVO"
        [ -f $ARCHIVO ] && rm $ARCHIVO
	return 1
fi

#Crea el archivo de suma de verificación
sha256sum $ARCHIVO | cut -d " " -f 1 > $SUMA_VER
if [ $? -eq 0 ]; then
        echo "Archivo $SUMA_VER creado"
	return 0
else
        echo "Error al crear $SUMA_VER"
        [ -f $ARCHIVO ] && rm $ARCHIVO
        [ -f $SUMA_VER ] && rm $SUMA_VER
	return 2
fi
