clear
#variable utilizada para salir del while
BANDERA=true

while $BANDERA
do
#clear
echo -e "\n"
echo 1 - "Generar Imágenes"
echo 2 - "Descargar Archivo Comprimido y su Suma de Verificación"
echo 3 - "Descomprimir Imágenes"
echo 4 - "Procesar Imágenes "
echo 5 - "Comprimir Imágenes"
echo 6 - "Salir"

read -p "Ingrese una opción: " OPCION
echo -e "\n"

case $OPCION in
        1)
		echo 1 - "Generar Imágenes Mujer/Hombre"
		echo 2 - "Generar Imágenes Personas"
		read -p "Ingrese una opción: " OPCION_GENERAR

		case $OPCION_GENERAR in
        		1)
				source generar.sh
			;;
                        2)
				source generar_personas.sh
                        ;;
                        *)
				echo "Opción no válida"
                        ;;
		esac
        ;;
        2)
                source descargar.sh
        ;;
	3)	
		source descomprimir.sh
	;;
	4)
		source procesar.sh
	;;
	5)
		source comprimir.sh
	;;	
        6)
                BANDERA=false
        ;;
        *)
                echo "Opción no válida"
        ;;

esac
done
exit 0

