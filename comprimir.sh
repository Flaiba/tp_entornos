#Comprueba que exista la carpeta imagenes
if [ ! -d imagenes ]; then
	echo "No existe la carpeta imagenes"	
	return 1
#comprueba que en la carpeta haya imágenes
elif [ $(find imagenes/ -type f -name '*.jpg' | wc -l) = 0 ]; then
	echo "La carpeta imagenes no tiene archivos .jpg"
	return 2
fi
cd imagenes/

#Si los archivos existen, los borra
rm nombres_todos.txt nombres_validos.txt nombres_fin_a.txt &> /dev/null

for ARCHIVO in *.jpg; do
        if [ -f $ARCHIVO ]; then
		#Quita .jpg (ej. Pedro.jpg -> Pedro)
                NOMBRE=$(echo $ARCHIVO | cut -d . -f 1)

		#Genera un archivo (nombres_todos.txt) con la lista de nombres de todas las imágenes
                echo $NOMBRE >> nombres_todos.txt
                
		#Genera un archivo (nombres_validos.txt) con la lista de nombres válidos (ej. Pedro, Isabel, Marta)
		if [[ $NOMBRE =~ [[:upper:]][[:lower:]]+ ]]; then
                        echo $NOMBRE >> nombres_validos.txt
                fi

		#Genera un archivo (nombres_fin_a.txt) con el total de personas cuyo nombre finaliza con la letra a (ej. Marta)
                if [[ $NOMBRE =~ [[:upper:]][[:lower:]]+a$ ]]; then
                        echo $NOMBRE >> nombres_fin_a.txt
                fi
        fi
done

ARCHIVO_FINAL="imagenes_texto"
#Si existen, los borra
rm $ARCHIVO_FINAL.tar $ARCHIVO_FINAL.tar.gz 2> /dev/null 

#Empaqueta los 3 archivos de texto generados y todas las imagenes de la carpeta imagenes  
tar -cf $ARCHIVO_FINAL.tar *.jpg nombres_todos.txt nombres_validos.txt nombres_fin_a.txt 2> /dev/null
if [ $? -eq 0 ]; then
        echo "Archivo $ARCHIVO_FINAL.tar creado"
else
        echo "Error al crear $ARCHIVO_FINAL.tar"
	rm $ARCHIVO_FINAL.tar
	cd ..
        return 3
fi

#Comprime el archivo empaquetado 
gzip $ARCHIVO_FINAL.tar 2> /dev/null 
if [ $? -eq 0 ]; then
        echo "Archivo $ARCHIVO_FINAL.tar.gz creado"
	#rm *.jpg *.txt
	cd ..
	return 0
else
        echo "Error al crear $ARCHIVO_FINAL.tar.gz"
	rm $ARCHIVO_FINAL.tar.gz
	cd ..        
	return 4
fi

