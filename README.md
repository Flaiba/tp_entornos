# Trabajo Práctico Final de Entorno de Programación 

Este documento describe el propósito y las características principales del trabajo práctico.  

Realizado bajo control de versiones con Gitlab.  
link del repositorio: https://gitlab.com/Flaiba/tp_entornos  

Se ejecuta dentro de un contenedor Docker. La carpeta 'imagenes' es accesible fuera del contenedor.  


## Descargar la imagen y crear un contenedor
- Descargar la imagen
    - docker pull flaiba/imagentp:V4
- Crear el contenedor
    - docker run -it -v ./imagenes:/tpentorno/imagenes flaiba/imagentp:V4


## Funcionamiento
El menú principal permite al usuario las siguientes opciones:  

### 1) Generar Imágenes
Descarga un número de imágenes determinado por el usuario y le asigna nombres válidos (comienzan con una letra mayúscula y el resto en minúsculas) aleatoriamente.  
Permite dos opciones:

    1) Generar Imágenes Mujer/Hombre
    Los nombres de mujer terminan con "a" y son asignados a imágenes de mujeres:
    link nombres de mujeres: https://raw.githubusercontent.com/marcboquet/spanish-names/master/mujeres.csv
    link imágenes de mujeres: https://source.unsplash.com/random/900%C3%97700/?woman

    Los nombres de hombre terminan con "o" y son asignados a imágenes de hombre: 
    link nombres de hombres: https://raw.githubusercontent.com/marcboquet/spanish-names/master/hombres.csv  
    link imágenes de hombres: https://source.unsplash.com/random/900%C3%97700/?man

    2) Generar Imágenes Personas
    Cualquier nombre válido es asignado a cualquier imagen
    link nombres: https://raw.githubusercontent.com/adalessandro/EdP-2023-TP-Final/main/dict.csv
    link imágenes: https://thispersondoesnotexist.com

Las imágenes:
- Se guardan temporariamente en la carpeta 'imagenes'.
- Se comprimen en el archivo 'imagenes.tar.gz' y se genera el archivo 'sumaDeVerificacion.txt' que contiene la suma de verificación del archivo comprimido.
- Se eliminan.  
En caso de éxito o error, se informa al usuario.  
![Generar](https://i.imgur.com/XM2i19u.jpg)

### 2) Descargar Archivo Comprimido y su Suma de Verificación
Solicita al usuario los links de descarga de un archivo comprimido y de su suma de verificación.  
Descarga los archivos como 'imagenes.tar.gz' y 'sumaDeVerificacion.txt', respectivamente.  
Verifica su integridad. En caso de error, los elimina.  
En caso de éxito o error, se informa al usuario.  

Algunos links de descarga para testeo:
- Imágenes con nombres separados por Mujeres y Hombres (comienzan con una letra mayúscula, el resto en minúsculas y terminan con 'a' u 'o')  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/imagenesMH.tar.gz?inline=false
- Suma de Verificación  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/sumaMH.txt?inline=false
- Suma de Verificación Errónea   
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/sumaMHErronea.txt?inline=false
  
- Imágenes con nombres de Personas (comienzan con una letra mayúscula y el resto en minúsculas)  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/imagenesPersonas.tar.gz?inline=false
- Suma de Verificación  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/sumaPersonas.txt?inline=false
- Suma de Verificación Errónea  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/sumaPersonasErronea.txt?inline=false
  
- Imágenes con nombres Mezclados (comienzan con mayúscula o minúscula)  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/imagenesMezcladas.tar.gz?inline=false
- Suma de Verificación  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/sumaMezcladas.txt?inline=false
- Suma de Verificación Errónea  
https://gitlab.com/Flaiba/imagenes_comprimidas/-/raw/main/sumaMezcladasErronea.txt?inline=false

### 3) Descomprimir Imágenes
Descomprime y desempaqueta el archivo 'imagenes.tar.gz' generado previamente por 1) 'Generar Imágenes' o  2)'Descargar Imágenes'.  
El contenido del archivo se extrae en la carpeta 'imagenes'.  
En caso de éxito o error, se informa al usuario.  
![Descomprimir](https://i.imgur.com/PyHKGZL.jpg)

### 4) Procesar Imágenes
Comprueba que exista la carpeta 'imagenes' y que contenga imágenes en formato JPG.  
Procesa las imágenes de la carpeta 'imagenes' recortándolas a una resolución de 512x512 con la herramienta convert de ImageMagick.  
![Procesar](https://i.imgur.com/RCwvVzY.jpg)

### 5) Comprimir Imágenes
Generar un archivo comprimido 'imagenes_texto.tar.gz' que incluye las imágenes procesadas y los siguientes archivos de texto:  
- nombres_todos.txt: lista de nombres de todas las imágenes.
- nombres_validos.txt: lista de nombres válidos (nombres de personas).
- nombres_fin_a.txt: lista de nombres que terminan con la letra "a".  
Se puede acceder al archivo comprimido desde fuera del contenedor en la carpeta 'imagenes'.  
En caso de éxito o error, se informa al usuario.  
![Procesar](https://i.imgur.com/YFcuGwc.jpg)

### 6) Salir
Termina la ejecución del programa.

---

## Desarrollo
- Generar los scripts: menu.sh, generar.sh, generar_personas.sh, descargar.sh, descomprimir.sh, procesar.sh, comprimir.sh
- Generar links con archivos para testeo: links_descargar_imagenes.txt
- Generar el archivo Dockefile  
 Imagen base: ubuntu:22.04 (versión actual al 08/07/23).  
 Actualiza los paquetes disponibles en el sistema e instala las siguientes herramientas: bash, wget, curl y imagemagick.  
- Crear la imagen
    - docker build -t imagentp .
- Crear el contenedor con el volumen 'imagenes' para permitir el intercambio de archivos con el host.
    - docker run -it -v ./imagenes:/tpentorno/imagenes imagentp
- Crear una cuenta en Docker Hub
- Subir la imagen a Docker Hub
    - docker login
    - docker tag imagentp flaiba/imagentp:V4
    - docker push flaiba/imagentp:V4

Otros comandos utilizados:
- Detener el contenedor
    - docker stop 'id_contenedor'
- Reiniciar el contenedor
    - docker start 'id_contenedor'
- Ejecutar un contenedor ya creado
    - docker exec -it 'id_contenedor' bash
    - docker exec -it 'id_contenedor' bash menu.sh

---

## Integrantes  
Bolañiuk Chiara  
Flaibani Marcela  
