#Comprueba que exista la carpeta imagenes
if [ ! -d imagenes ]; then
        echo "No existe la carpeta imagenes"    
        return 1
#Comprueba que en la carpeta haya imágenes
elif [ $(find imagenes/ -type f -name '*.jpg' | wc -l) -eq 0 ]; then
        echo "La carpeta imagenes no tiene archivos .jpg"
        return 2
fi

#Recorta las imágenes que tengan nombre de persona (ej. Pedro.jpg, Isabel.jpg, Marta.jpg) a una resolución de 512*512
for ARCHIVO in imagenes/[A-Z][a-z]*.jpg; do
	if [ -f $ARCHIVO ]; then
		convert $ARCHIVO -gravity center -resize 512x512+0+0 \
		-extent 512x512 $ARCHIVO
	fi
done
return 0
